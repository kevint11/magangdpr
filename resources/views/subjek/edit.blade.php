@extends('layouts.app')

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div>
            <ol class="breadcrumb fs-sm mb-1">
                <li class="breadcrumb-item">Menu</li>
                <li class="breadcrumb-item"><a href="{{ route('Daftar Subjek') }}">Daftar Subjek</a></li>
            </ol>
            <h4 class="main-title mb-0">{{ !empty($subjek) ? 'Edit' : 'Tambah' }} Subjek</h4>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success" id="success-alert">
                    {{ session('success') }}
                </div>
                <script>
                    setTimeout(function() {
                        document.getElementById('success-alert').remove();
                    }, 3000);
                </script>
            @endif
            <div class="mb-3">
                    <form id="FormEditSubjek"  method="POST" enctype="multipart/form-data"
                        action="{{ !empty($subjek) ? route('Edit Subjek', $subjek->id) : route('Tambah Subjek') }}">
                        @csrf

                    <input type="hidden" name="id" id="id" value="{{ !empty($subjek) ? $subjek->id : '' }}"/>
                    <div class="mb-3">
                        <label for="subjek" class="form-label fw-bold">Subjek</label>
                        <div class="d-flex flex-row gap-2">
                            <input required type="text" class="form-control w-50" id="subjek" name="subjek"
                                placeholder="Masukan Subjek" value="{{ !empty($subjek) ? $subjek->subjek : '' }}">
                            <font style="color: red; display: flex; align-items: center; padding: 0;">*</font>
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="subjek_en" class="form-label fw-bold">Subjek EN </label>
                        <div class="d-flex flex-row gap-2">
                            <input required type="text" class="form-control w-50" id="subjek_en" name="subjek_en"
                                placeholder="Masukkan Tautan subjek_en" value="{{ !empty($subjek) ? $subjek->subjek_en : '' }}">
                            <font style="color: red; display: flex; align-items: center; padding: 0;">*</font>
                        </div>
                    </div>

                    <input type="submit" value="Simpan Perubahan" class="btn btn-primary">
                </form>
            </div>
        </div>
    </div>
@endsection

@push('script')

@endpush
