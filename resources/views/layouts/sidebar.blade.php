

<div class="sidebar">
    <div class="sidebar-header">
        <a href="{{ asset('theme/admin-dashbyte/dist/') }}" class="sidebar-logo">E - Paper</a>

    </div>
    <div id="sidebarMenu" class="sidebar-body">
        <div class="nav-group show">
            <a href="#" class="nav-label">Dashboard</a>
            <ul class="nav nav-sidebar">
                    <li class="nav-item">
                        <a href="{{ asset("/") }}" class="nav-link {{ request()->is("/") ? ' active' : '' }}"><i
                                class="ri-pie-chart-2-line"></i> <span>Dashboard E - Paper</span></a>
                    </li>

            </ul>
        </div>

        <div class="nav-group show">
            <a href="#" class="nav-label">Data Master</a>
            <ul class="nav nav-sidebar">
                <li class="nav-item">
                    <a href="{{ asset("/surat") }}"
                        class="nav-link {{ request()->is("/surat*") ? ' active' : '' }}"><i
                            class="ri-newspaper-line"></i> <span>Surat Kabar</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ asset("/subjek") }}"
                        class="nav-link {{ request()->is("/subjek*") ? ' active' : '' }}"><i
                            class="ri-calendar-event-line"></i> <span>Subjek</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ asset("/running") }}"
                        class="nav-link {{ request()->is("/running*") ? ' active' : '' }}"><i
                            class="ri-artboard-line"></i> <span>Running Text</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ asset("/hari-libur") }}"
                        class="nav-link {{ request()->is("/hari-libur*") ? ' active' : '' }}"><i
                            class="ri-file-text-line"></i> <span>Alat Kelengkapan Dewan</span></a>
                </li>
            </ul>
        </div>

        <div class="nav-group show">
            <a href="#" class="nav-label">Data E-Paper</a>
            <ul class="nav nav-sidebar">
                <li class="nav-item">
                    <a href="{{ asset("/epaper") }}"
                        class="nav-link {{ request()->is("/epaper*") ? ' active' : '' }}"><i
                            class="ri-newspaper-line"></i> <span>E-Paper</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ asset("/menu-akd") }}"
                        class="nav-link {{ request()->is("/menu-akd*") ? ' active' : '' }}"><i
                            class="ri-calendar-event-line"></i> <span>Rekap E-Paper</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ asset("/masa-persidangan") }}"
                        class="nav-link {{ request()->is("/masa-persidangan*") ? ' active' : '' }}"><i
                            class="ri-artboard-line"></i> <span>Pencarian</span></a>
                </li>
            </ul>
        </div>

        <div class="nav-group show">
            <a href="#" class="nav-label">Data Laporan</a>
            <ul class="nav nav-sidebar">
                <li class="nav-item">
                    <a href="{{ asset("/menu") }}"
                        class="nav-link {{ request()->is("/menu*") ? ' active' : '' }}"><i
                            class="ri-newspaper-line"></i> <span>Laporan</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ asset("/menu-akd") }}"
                        class="nav-link {{ request()->is("/menu-akd*") ? ' active' : '' }}"><i
                            class="ri-calendar-event-line"></i> <span>Rekapitulasi</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ asset("/masa-persidangan") }}"
                        class="nav-link {{ request()->is("/masa-persidangan*") ? ' active' : '' }}"><i
                            class="ri-artboard-line"></i> <span>Statistik</span></a>
                </li>
            </ul>
        </div>
    </div>

    <div class="sidebar-footer">
        <div class="sidebar-footer-top">
            <div class="sidebar-footer-thumb">
                @if (session('informal_photo_name'))
                        <img src="https://berkas.dpr.go.id/portal/photos/{{ session('informal_photo_name') }}"
                            alt="Foto Profil">
                    @else
                    <img src="{{ asset('theme/admin-dashbyte/dist/assets/img/user.png') }}" alt="Foto Profil">
                    @endif
            </div>
            <div class="sidebar-footer-body">
                <h6><a href="#">{{ session('nama') }}</a></h6>
                <p>admin</p>
            </div>
            <a id="sidebarFooterMenu" href="" class="dropdown-link"><i class="ri-arrow-down-s-line"></i></a>
        </div>
        <div class="sidebar-footer-menu">
            <nav class="nav">
                {{-- <a href="{{ route('profil.edit', 0) }}"><i class="ri-edit-2-line"></i> Sunting Profil</a>
                <a href="{{ route('profil.index') }}"><i class="ri-profile-line"></i> Tampilkan Profil</a> --}}
            </nav>
            <hr>
            <nav class="nav">
                <a href=""><i class="ri-question-line"></i> Pusat Bantuan</a>
                <a href=""><i class="ri-lock-line"></i> Pengaturan Privasi</a>
                <a href=""><i class="ri-user-settings-line"></i> Pengaturan Akun</a>
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="ri-logout-box-r-line"></i> Keluar
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </nav>
        </div>
    </div>
</div>
