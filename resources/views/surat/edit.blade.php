@extends('layouts.app')

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div>
            <ol class="breadcrumb fs-sm mb-1">
                <li class="breadcrumb-item">Menu</li>
                <li class="breadcrumb-item"><a href="{{ route('Daftar Surat') }}">Daftar Surat Kabar</a></li>
            </ol>
            <h4 class="main-title mb-0">{{ !empty($surat) ? 'Edit' : 'Tambah' }} Surat Kabar</h4>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success" id="success-alert">
                    {{ session('success') }}
                </div>
                <script>
                    setTimeout(function() {
                        document.getElementById('success-alert').remove();
                    }, 3000);
                </script>
            @endif
            <div class="mb-3">
                    <form id="FormEditSurat"  method="POST" enctype="multipart/form-data"
                        action="{{ !empty($surat) ? route('Edit Surat', $surat->id) : route('Tambah Surat') }}">
                        @csrf

                    <input type="hidden" name="id" id="id" value="{{ !empty($surat) ? $surat->id : '' }}"/>
                    <div class="mb-3">
                        <label for="surat_kabar" class="form-label fw-bold">Surat Kabar</label>
                        <div class="d-flex flex-row gap-2">
                            <input required type="text" class="form-control w-50" id="surat_kabar" name="surat_kabar"
                                placeholder="Masukan Surat Kabar" value="{{ !empty($surat) ? $surat->surat_kabar : '' }}">
                            <font style="color: red; display: flex; align-items: center; padding: 0;">*</font>
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="url" class="form-label fw-bold">Link (URL) </label>
                        <div class="d-flex flex-row gap-2">
                            <input required type="text" class="form-control w-50" id="url" name="url"
                                placeholder="Masukkan Tautan URL" value="{{ !empty($surat) ? $surat->url : '' }}">
                            <font style="color: red; display: flex; align-items: center; padding: 0;">*</font>
                        </div>
                    </div>

                    <input type="submit" value="Simpan Perubahan" class="btn btn-primary">
                </form>
            </div>
        </div>
    </div>
@endsection

@push('script')

@endpush
