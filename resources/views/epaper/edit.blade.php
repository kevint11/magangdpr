@extends('layouts.app')

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div>
            <ol class="breadcrumb fs-sm mb-1">
                <li class="breadcrumb-item">Menu</li>
                <li class="breadcrumb-item"><a href="{{ route('Daftar Epaper') }}">Daftar Koran Digital</a></li>
            </ol>
            <h4 class="main-title mb-0">{{ !empty($epaper) ? 'Edit' : 'Tambah' }} Koran Digital</h4>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success" id="success-alert">
                    {{ session('success') }}
                </div>
                <script>
                    setTimeout(function() {
                        document.getElementById('success-alert').remove();
                    }, 3000);
                </script>
            @endif
            <div class="mb-3">
                    <form id="FormEditEpaper"  method="POST" enctype="multipart/form-data"
                        action="{{ !empty($epaper) ? route('Edit Epaper', $epaper->id) : route('Tambah Epaper') }}">
                        @csrf

                    <input type="hidden" name="id" id="id" value="{{ !empty($epaper) ? $epaper->id : '' }}"/>
                    <div class="mb-3">
                        <label for="judul" class="form-label fw-bold">Judul</label>
                        <div class="d-flex flex-row gap-2">
                            <input required type="text" class="form-control w-50" id="judul" name="judul"
                                placeholder="Masukkan Judul" value="{{ !empty($epaper) ? $epaper->judul : '' }}">
                            <font style="color: red; display: flex; align-items: center; padding: 0;">*</font>
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="tanggal" class="form-label fw-bold">Tanggal</label>
                        <div class="d-flex flex-row gap-2">
                            <input required type="date" class="form-control w-50" id="tanggal" name="tanggal"
                                placeholder="Masukkan Tanggal" value="{{ !empty($epaper) ? $epaper->tanggal : '' }}">
                            <font style="color: red; display: flex; align-items: center; padding: 0;">*</font>
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="id_surat_kabar" class="form-label fw-bold">Surat Kabar</label>
                        <div class="d-flex flex-row gap-2">
                            <select class="form-control w-50" required aria-label="Masukkan Surat Kabar" name="id_surat_kabar" id="id_surat_kabar">
                                <option disabled selected>Pilih Surat Kabar</option>
                                <option value="1" {{ !empty($epaper) && $epaper->id_surat_kabar == '1'  ? 'selected' : '' }}>Kompas</option>
                                <option value="2" {{ !empty($epaper) && $epaper->id_surat_kabar == '2'  ? 'selected' : '' }}>Republika</option>
                                <option value="3" {{ !empty($epaper) && $epaper->id_surat_kabar == '3'  ? 'selected' : '' }}>Tempo</option>
                            </select>
                            <font style="color: red; display: flex; align-items: center; padding: 0;">*</font>
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="halaman" class="form-label fw-bold">Halaman</label>
                        <div class="d-flex flex-row gap-2">
                            <input required type="text" class="form-control w-50" id="halaman" name="halaman"
                                placeholder="Masukkan Halaman" value="{{ !empty($epaper) ? $epaper->halaman : '' }}">
                            <font style="color: red; display: flex; align-items: center; padding: 0;">*</font>
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="url" class="form-label fw-bold">Tautal Url</label>
                        <div class="d-flex flex-row gap-2">
                            <input required type="text" class="form-control w-50" id="url" name="url"
                                placeholder="Masukkan Tautal Url" value="{{ !empty($epaper) ? $epaper->url : '' }}">
                            <font style="color: red; display: flex; align-items: center; padding: 0;">*</font>
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="subjek" class="form-label fw-bold">Subjek</label>
                        <div class="d-flex flex-row gap-2">
                            <input required type="text" class="form-control w-50" id="subjek" name="subjek"
                                placeholder="Masukkan Subjek" value="{{ !empty($epaper) ? $epaper->url : '' }}">
                            <font style="color: red; display: flex; align-items: center; padding: 0;">*</font>
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="isi" class="form-label fw-bold">Isi</label>
                        <div class="d-flex flex-row gap-2">
                            <textarea required class="form-control w-50" id="isi" name="isi">
                                {{ !empty($epaper->isi) ? $epaper->isi : '' }}
                            </textarea>
                            <font style="color: red; display: flex; align-items: center; padding: 0;">*</font>
                        </div>
                    </div>

                    <input type="submit" value="Simpan Perubahan" class="btn btn-primary">
                </form>
            </div>
        </div>
    </div>
@endsection

@push('script')

@endpush
