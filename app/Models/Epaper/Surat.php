<?php

namespace App\Models\Epaper;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Surat extends Model
{
    use HasFactory;

    protected $table= 'surat_kabar';
    public $incrementing = true;
    protected $guarded = [];
    public $timestamps = false;

}
