<?php

namespace App\Models\Epaper;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Epaper extends Model
{
    use HasFactory;

    protected $table= 'epaper';
    public $incrementing = true;
    protected $guarded = [];
    public $timestamps = false;

}
