<?php

namespace App\Models\Epaper;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Running extends Model
{
    use HasFactory;

    protected $table= 'marquee';
    public $incrementing = true;
    protected $guarded = [];
    public $timestamps = false;

}
