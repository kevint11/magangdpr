<?php

namespace App\Models\Epaper;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subjek extends Model
{
    use HasFactory;

    protected $table= 'subjek';
    public $incrementing = true;
    protected $guarded = [];
    public $timestamps = false;

}
