<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Epaper\Surat;
use Exception;
use Illuminate\Support\Facades\DB;

class SuratController extends Controller
{
    public function tblSurat()
    {
        $surat = Surat::where('status','1')->paginate(10);
        return view('surat.index',compact('surat'));
    }

    public function addSurat()
    {
        return view('surat.edit');
    }

    public function tambahSurat(Request $request)
    {
        $validation = $request->validate([
            'surat_kabar'=>['required', 'string'],
            'url'=>['required', 'string'],
        ]);

        DB::beginTransaction();
        $return_status = 'Valid';

        try {
            $surat = Surat::create([
                'surat_kabar'=>$validation['surat_kabar'],
                'url'=>$validation['url'],
                'status'=>'1',
                'user_input'=>auth()->user()->id,
                'tanggal_input'=>date('Y-m-d H:i:s'),
            ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error',$e);
        }

        return redirect()->route('Daftar Surat');
    }

    public function editSurat($id)
    {
        $surat = Surat::findOrFail($id);
        return view('surat.edit', compact('surat'));
    }


    public function updateSurat(Request $request, $id)
    {
        $validation = $request->validate([
            'surat_kabar'=>['required', 'string'],
            'url'=>['required', 'string'],
        ]);

        DB::beginTransaction();
        $return_status = 'Valid';

        try {
            $surat = Surat::findOrFail($id);
            $surat->update([
                'surat_kabar'=>$validation['surat_kabar'],
                'url'=>$validation['url'],
                'status'=>'1',
                'user_update'=>auth()->user()->id,
                'tanggal_update'=>date('Y-m-d H:i:s'),
            ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error',$e);
        }

        return redirect()->route('Daftar Surat');
    }

    public function deleteSurat($id)
    {
        $surat = Surat::findOrFail($id);
        $surat->update([
            'status'=>'0',
            'user_update'=>auth()->user()->id,
            'tanggal_update'=>date('Y-m-d H:i:s'),
        ]);
        return redirect()->route('Daftar Surat')->with('success', 'Surat berhasil dihapus');
    }
}
