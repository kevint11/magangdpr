<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Epaper\Running;
use Exception;
use Illuminate\Support\Facades\DB;

class RunningController extends Controller
{
    public function tblRunning()
    {
        $running = Running::where('status','1')->paginate(10);
        return view('running.index',compact('running'));
    }

    public function addRunning()
    {
        return view('running.edit');
    }

    public function tambahRunning(Request $request)
    {
        $validation = $request->validate([
            'teks'=>['required', 'string'],
        ]);

        DB::beginTransaction();
        $return_status = 'Valid';

        try {
            $running = Running::create([
                'teks'=>$validation['teks'],
                'status'=>'1',
                'user_input'=>auth()->user()->id,
                'tanggal_input'=>date('Y-m-d H:i:s'),
            ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error',$e);
        }

        return redirect()->route('Daftar Running');
    }

    public function editRunning($id)
    {
        $running = Running::findOrFail($id);
        return view('running.edit', compact('running'));
    }


    public function updateRunning(Request $request, $id)
    {
        $validation = $request->validate([
            'teks'=>['required', 'string'],
        ]);

        DB::beginTransaction();
        $return_status = 'Valid';

        try {
            $running = Running::findOrFail($id);
            $running->update([
                'teks'=>$validation['teks'],
                'status'=>'1',
                'user_update'=>auth()->user()->id,
                'tanggal_update'=>date('Y-m-d H:i:s'),
            ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error',$e);
        }

        return redirect()->route('Daftar Running');
    }

    public function deleteRunning($id)
    {
        $running = Running::findOrFail($id);
        $running->update([
            'status'=>'0',
            'user_update'=>auth()->user()->id,
            'tanggal_update'=>date('Y-m-d H:i:s'),
        ]);
        return redirect()->route('Daftar Running')->with('success', 'running berhasil dihapus');
    }
}
