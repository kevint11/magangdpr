<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Epaper\Subjek;
use Exception;
use Illuminate\Support\Facades\DB;

class SubjekController extends Controller
{
    public function tblSubjek()
    {
        $subjek = Subjek::where('status','1')->paginate(10);
        return view('subjek.index',compact('subjek'));
    }

    public function addSubjek()
    {
        return view('subjek.edit');
    }

    public function tambahSubjek(Request $request)
    {
        $validation = $request->validate([
            'subjek'=>['required', 'string'],
            'subjek_en'=>['required', 'string'],
        ]);

        DB::beginTransaction();
        $return_status = 'Valid';

        try {
            $subjek = Subjek::create([
                'subjek'=>$validation['subjek'],
                'subjek_en'=>$validation['subjek_en'],
                'status'=>'1',
                'user_input'=>auth()->user()->id,
                'tanggal_input'=>date('Y-m-d H:i:s'),
            ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error',$e);
        }

        return redirect()->route('Daftar Subjek');
    }

    public function editSubjek($id)
    {
        $subjek = Subjek::findOrFail($id);
        return view('subjek.edit', compact('subjek'));
    }


    public function updateSubjek(Request $request, $id)
    {
        $validation = $request->validate([
            'subjek'=>['required', 'string'],
            'subjek_en'=>['required', 'string'],
        ]);

        DB::beginTransaction();
        $return_status = 'Valid';

        try {
            $subjek = Subjek::findOrFail($id);
            $subjek->update([
                'subjek'=>$validation['subjek'],
                'subjek_en'=>$validation['subjek_en'],
                'status'=>'1',
                'user_update'=>auth()->user()->id,
                'tanggal_update'=>date('Y-m-d H:i:s'),
            ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error',$e);
        }

        return redirect()->route('Daftar Subjek');
    }

    public function deleteSubjek($id)
    {
        $subjek = Subjek::findOrFail($id);
        $subjek->update([
            'status'=>'0',
            'user_update'=>auth()->user()->id,
            'tanggal_update'=>date('Y-m-d H:i:s'),
        ]);
        return redirect()->route('Daftar Subjek')->with('success', 'subjek berhasil dihapus');
    }
}
