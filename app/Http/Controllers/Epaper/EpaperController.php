<?php

namespace App\Http\Controllers\Epaper;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Epaper\Epaper;
use Exception;
use Illuminate\Support\Facades\DB;

class EpaperController extends Controller
{
    public function tblEpaper()
    {
        $epaper = Epaper::where('status','1')->paginate(10);
        return view('epaper.index',compact('epaper'));
    }

    public function addEpaper()
    {
        return view('epaper.edit');
    }

    public function tambahEpaper(Request $request)
    {
        $validation = $request->validate([
            'judul'=>['required', 'string'],
            'id_surat_kabar'=>['required', 'integer'],
            'halaman'=>['required', 'string'],
            'tanggal'=>['required', 'string'],
            'url'=>['required', 'string'],
            'isi'=>['required', 'string'],
        ]);

        DB::beginTransaction();
        $return_status = 'Valid';

        try {
            $epaper = Epaper::create([
                'judul'=>$validation['judul'],
                'id_surat_kabar'=>$validation['id_surat_kabar'],
                'halaman'=>$validation['halaman'],
                'kata_kunci'=>'keyword',
                'tanggal'=>$validation['tanggal'],
                'url'=>$validation['url'],
                'isi'=>$validation['isi'],
                'status'=>'1',
                'user_input'=>auth()->user()->id,
                'tanggal_input'=>date('Y-m-d H:i:s'),
            ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error',$e);
        }

        return redirect()->route('Daftar Epaper');
    }

    public function editEpaper($id)
    {
        $epaper = Epaper::findOrFail($id);
        return view('epaper.edit', compact('epaper'));
    }


    public function updateEpaper(Request $request, $id)
    {
        $validation = $request->validate([
            'judul'=>['required', 'string'],
            'id_surat_kabar'=>['required', 'string'],
            'halaman'=>['required', 'string'],
            'tanggal'=>['required', 'string'],
            'url'=>['required', 'string'],
            'isi'=>['required', 'string'],
        ]);

        DB::beginTransaction();
        $return_status = 'Valid';

        try {
            $epaper = Epaper::findOrFail($id);
            $epaper->update([
                'judul'=>$validation['judul'],
                'id_surat_kabar'=>$validation['id_surat_kabar'],
                'halaman'=>$validation['halaman'],
                'tanggal'=>$validation['tanggal'],
                'url'=>$validation['url'],
                'isi'=>$validation['isi'],
                'status'=>'1',
                'user_update'=>auth()->user()->id,
                'tanggal_update'=>date('Y-m-d H:i:s'),
            ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error',$e);
        }

        return redirect()->route('Daftar Epaper');
    }

    public function deleteEpaper($id)
    {
        $epaper = Epaper::findOrFail($id);
        $epaper->update([
            'status'=>'0',
            'user_update'=>auth()->user()->id,
            'tanggal_update'=>date('Y-m-d H:i:s'),
        ]);
        return redirect()->route('Daftar Epaper')->with('success', 'epaper berhasil dihapus');
    }
}
