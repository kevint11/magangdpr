<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\MenuController;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;

// use Ramsey\Collection\Map\NamedParameterMap;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login')->middleware('guest');

Route::post('/login', [LoginController::class, 'login']);
Route::post('/logout', function () {
    Auth::logout();
    return redirect('/login');
})->name('logout');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', [AdminController::class, 'index'])->name('admin.index');
    Route::resource('menu', MenuController::class);

    Route::get('/surat', [App\Http\Controllers\Master\SuratController::class, 'tblSurat'])->name('Daftar Surat');
    Route::get('/surat/add', [App\Http\Controllers\Master\SuratController::class, 'addSurat'])->name('Add Surat');
    Route::post('/surat/add', [App\Http\Controllers\Master\SuratController::class, 'tambahSurat'])->name('Tambah Surat');
    Route::get('/surat/{id}', [App\Http\Controllers\Master\SuratController::class, 'editSurat'])->name('Edit Surat');
    Route::post('/surat/{id}', [App\Http\Controllers\Master\SuratController::class, 'updateSurat'])->name('Update Surat');
    Route::put('/surat/{id}', [App\Http\Controllers\Master\SuratController::class, 'deleteSurat'])->name('Hapus Surat');


    Route::get('/subjek', [App\Http\Controllers\Master\SubjekController::class, 'tblSubjek'])->name('Daftar Subjek');
    Route::get('/subjek/add', [App\Http\Controllers\Master\SubjekController::class, 'addSubjek'])->name('Add Subjek');
    Route::post('/subjek/add', [App\Http\Controllers\Master\SubjekController::class, 'tambahSubjek'])->name('Tambah Subjek');
    Route::get('/subjek/{id}', [App\Http\Controllers\Master\SubjekController::class, 'editSubjek'])->name('Edit Subjek');
    Route::post('/subjek/{id}', [App\Http\Controllers\Master\SubjekController::class, 'updateSubjek'])->name('Update Subjek');
    Route::put('/subjek/{id}', [App\Http\Controllers\Master\SubjekController::class, 'deleteSubjek'])->name('Hapus Subjek');

    Route::get('/running', [App\Http\Controllers\Master\RunningController::class, 'tblRunning'])->name('Daftar Running');
    Route::get('/running/add', [App\Http\Controllers\Master\RunningController::class, 'addRunning'])->name('Add Running');
    Route::post('/running/add', [App\Http\Controllers\Master\RunningController::class, 'tambahRunning'])->name('Tambah Running');
    Route::get('/running/{id}', [App\Http\Controllers\Master\RunningController::class, 'editRunning'])->name('Edit Running');
    Route::post('/running/{id}', [App\Http\Controllers\Master\RunningController::class, 'updateRunning'])->name('Update Running');
    Route::put('/running/{id}', [App\Http\Controllers\Master\RunningController::class, 'deleteRunning'])->name('Hapus Running');


    Route::get('/epaper', [App\Http\Controllers\Epaper\EpaperController::class, 'tblEpaper'])->name('Daftar Epaper');
    Route::get('/epaper/add', [App\Http\Controllers\Epaper\EpaperController::class, 'addEpaper'])->name('Add Epaper');
    Route::post('/epaper/add', [App\Http\Controllers\Epaper\EpaperController::class, 'tambahEpaper'])->name('Tambah Epaper');
    Route::get('/epaper/{id}', [App\Http\Controllers\Epaper\EpaperController::class, 'editEpaper'])->name('Edit Epaper');
    Route::post('/epaper/{id}', [App\Http\Controllers\Epaper\EpaperController::class, 'updateEpaper'])->name('Update Epaper');
    Route::put('/epaper/{id}', [App\Http\Controllers\Epaper\EpaperController::class, 'deleteEpaper'])->name('Hapus Epaper');

});
